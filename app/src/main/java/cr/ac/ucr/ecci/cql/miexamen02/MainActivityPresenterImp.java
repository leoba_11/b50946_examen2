package cr.ac.ucr.ecci.cql.miexamen02;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.MainActivityPresenter;
import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.MainActivityView;
import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.TableTopInteractor;

public class MainActivityPresenterImp implements MainActivityPresenter, TableTopInteractor.OnFinishedListener {

    private MainActivityView mMainActivityView;
    private TableTopInteractor tableTopInteractor;

    public MainActivityPresenterImp(MainActivityView mainActivityView){
        this.mMainActivityView = mainActivityView;

        // Capa de negocios (Interactor)
        this.tableTopInteractor = new TableTopInteractorImp();
    }

    @Override
    public void onResume(){
//        if(mMainActivityView!=null){
//            mMainActivityView.showProgress();
//        }
        // Obtener los items de la capa de negocios (Interactor)
        tableTopInteractor.getItems(this);
    }

    @Override
    public void onItemClicked(TableTop item) {

    }

    @Override
    public void onDestroy(){
        mMainActivityView=null;
    }

    @Override
    public void onFinished(List<TableTop> items){
        if(mMainActivityView!=null){
            mMainActivityView.setItems(items);
//            mMainActivityView.hideProgress();
        }
    }
}
