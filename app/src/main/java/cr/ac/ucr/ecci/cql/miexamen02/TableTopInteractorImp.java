package cr.ac.ucr.ecci.cql.miexamen02;

import android.os.Handler;

import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.IServiceDataSource;
import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.TableTopInteractor;

public class TableTopInteractorImp implements TableTopInteractor {

    private IServiceDataSource iServiceDataSource;


    @Override public void getItems(final OnFinishedListener listener) {
        // Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                iServiceDataSource = new IServiceDataSourceImp(listener);
                try {
                    // obtenemos los items
                    iServiceDataSource.obtainItems();
                } catch (CantRetrieveItemsException e) {
                    e.printStackTrace();
                }
            }
        }, 0);
    }

}
