package cr.ac.ucr.ecci.cql.miexamen02.Interfaces;

import cr.ac.ucr.ecci.cql.miexamen02.CantRetrieveItemsException;

public interface IServiceDataSource {
    void obtainItems() throws CantRetrieveItemsException;
}
