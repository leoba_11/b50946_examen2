package cr.ac.ucr.ecci.cql.miexamen02;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TableTopFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TableTopFragment extends Fragment {

    private TableTop item;

    public TableTopFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public static TableTopFragment newInstance(int index) {
        TableTopFragment f = new TableTopFragment();
        // Provee el index como argumento para mostrar el detalle de datos.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if(bundle != null){
            this.item = bundle.getParcelable("currentItem");
        }
        return inflater.inflate(R.layout.fragment_table_top, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.setTextValues(view);
    }

    public void setTextValues(View view){
        TextView nombre = view.findViewById(R.id.TableTopName);
        nombre.setText(item.getNombre());

        TextView identificacion = view.findViewById(R.id.ID);
        identificacion.setText(item.getIdentificacion());

        TextView year = view.findViewById(R.id.year);
        year.setText(item.getYear().toString());

        TextView publisher = view.findViewById(R.id.publisher);
        publisher.setText(item.getPublisher());
    }
}