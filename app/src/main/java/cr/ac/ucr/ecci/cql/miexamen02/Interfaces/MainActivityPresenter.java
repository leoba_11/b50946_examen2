package cr.ac.ucr.ecci.cql.miexamen02.Interfaces;

import cr.ac.ucr.ecci.cql.miexamen02.TableTop;

public interface MainActivityPresenter {

    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(TableTop item);
    // destruir
    void onDestroy();

}
