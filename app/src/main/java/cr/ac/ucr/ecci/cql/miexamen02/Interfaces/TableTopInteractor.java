package cr.ac.ucr.ecci.cql.miexamen02.Interfaces;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.TableTop;

public interface TableTopInteractor {

    interface OnFinishedListener {
        void onFinished(List<TableTop> items);
    }

    void getItems(OnFinishedListener listener);
}
