package cr.ac.ucr.ecci.cql.miexamen02;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.IServiceDataSource;
import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.TableTopInteractor;

public class IServiceDataSourceImp implements IServiceDataSource {

    private static final String URL_JSON = "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop4.json";
    private static final String TAG_IMG = "IMG";

    public TaskServicioREST task = new TaskServicioREST();
    private List<TableTop> items = null;
    private TableTopInteractor.OnFinishedListener mListener;

    public IServiceDataSourceImp(final TableTopInteractor.OnFinishedListener listener){
        mListener = listener;
    }

    @Override
    public void obtainItems() throws CantRetrieveItemsException {
        try {
            task.execute(URL_JSON);

        } catch (Exception e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
    }

    //Método para extraer solo el arreglo del json
    public String getSplitJson(String json){

        System.out.println("jasjsjadkjeidineindienidneindiendi" + json);

        int inicio = json.indexOf("[");
        int fin = json.length() - 1;
        return json.substring(inicio, fin);
    }

    public void postExecuteHelper(String json){
        String data = getSplitJson(json);
        Gson gson = new Gson();
        TableTop[] jsonObject = gson.fromJson(data, TableTop[].class);
        items = new ArrayList<>(Arrays.asList(jsonObject));
        mListener.onFinished(items);
    }


    // Clase para la actividad asynnc
    public class TaskServicioREST extends AsyncTask<String, Float, String> {

        private ProgressDialog mDialog;
        private String aux;
        public TaskServicioREST(){
            mDialog = MainActivity.getmDialog();
        }

        @Override
        protected void onPreExecute() {
            mDialog.setProgress(0);
        }

        @Override
        protected String doInBackground(String... urls) {
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {}
                publishProgress(i/100f);
            }
            return loadContentFromNetwork(urls[0]);
        }

        protected void onProgressUpdate (Float... values) {
            // actualizamos la barra de progreso y sus valores
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);
        }

        protected void onPostExecute(String bytes) {
            // Cuando la tarea termina cerramos la barra de progreso
            mDialog.dismiss();
            postExecuteHelper(aux);
        }

        private String loadContentFromNetwork(String url) {
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder(); String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }
                aux = strBuilder.toString();
                return strBuilder.toString();
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }

}
