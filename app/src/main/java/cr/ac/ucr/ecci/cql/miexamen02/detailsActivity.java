package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.Bundle;

public class detailsActivity extends AppCompatActivity {

    private TableTop tableTop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        tableTop = getIntent().getParcelableExtra("tableTop");
        setFragment(tableTop);
    }

    public void setFragment(TableTop item) {
        Bundle bundle = new Bundle();
        TableTopFragment details = new TableTopFragment();
        bundle.putParcelable("currentItem", item);
        details.setArguments(bundle);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.tableTops_details, details);
        ft.commit();
    }
}