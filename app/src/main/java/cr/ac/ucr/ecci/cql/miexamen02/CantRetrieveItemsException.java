package cr.ac.ucr.ecci.cql.miexamen02;

public class CantRetrieveItemsException extends Exception {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
