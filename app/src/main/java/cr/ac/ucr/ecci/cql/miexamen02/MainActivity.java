package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.MainActivityPresenter;
import cr.ac.ucr.ecci.cql.miexamen02.Interfaces.MainActivityView;

public class MainActivity extends AppCompatActivity implements MainActivityView, AdapterView.OnItemClickListener {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;
    private static ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Executing task...");
        mDialog.setTitle("Progress");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();

        mProgressBar = findViewById(R.id.progress);
        mMainActivityPresenter = new MainActivityPresenterImp(this);
    }

    // Mostrar los items de la lista en la UI
    @Override
    public void setItems(List<TableTop> items) {
        LazyAdapter mAdapter = new LazyAdapter(items, this);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        TableTop item = (TableTop) mListView.getAdapter().getItem(position);

        Intent detailActivity = new Intent(MainActivity.this, detailsActivity.class);
        detailActivity.putExtra("tableTop", item);
        startActivity(detailActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }

    // Esconder el indicador de progreso de la UI
    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }


    public static ProgressDialog getmDialog(){
        return mDialog;
    }

}